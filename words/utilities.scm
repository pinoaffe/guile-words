(define-module (words utilities)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:export (set-size
            fix-first
            multi-filter
            integer-memoize
            listify))

(define (set-size lst)
  (let ((bins (make-hash-table)))
    (for-each (lambda (element)
                (unless (hash-ref bins element #f)
                  (hash-set! bins element #t)))
              lst)
    (length (hash-map->list cons bins))))

(define* (fix-first value #:optional (f #f))
  (if f
      (lambda (. rest) (apply f value rest))
      (fix-first value fix-first)))

(define (multi-filter pred . lists)
  (apply values
         (apply map list
                (filter (fix-first pred apply)
                        (apply map list lists)))))

(define (integer-memoize f . bounds)
  (let ((memory (apply make-array #f bounds)))
    (lambda (. indexes)
      (let ((value (or (and (apply array-in-bounds? memory indexes)
                            (apply array-ref memory indexes))
                       (apply f indexes))))
        (when (apply array-in-bounds? memory indexes)
          (apply array-set! memory value indexes))
        value))))

(define (listify x)
  (if (string? x)
      (string->list x)
      x))
