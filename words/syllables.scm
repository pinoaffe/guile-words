(define-module (words syllables)
  #:use-module (srfi srfi-1)
  #:use-module (rx irregex)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 popen)
  #:export (syllables))

(define problematic
  '[
    ("abalone"          . 4)
    ("abare"            . 3)
    ("abbruzzese"       . 4)
    ("abed"             . 2)
    ("aborigine"        . 5)
    ("abruzzese"        . 4)
    ("acreage"          . 3)
    ("adame"            . 3)
    ("adieu"            . 2)
    ("adobe"            . 3)
    ("anemone"          . 4)
    ("apache"           . 3)
    ("aphrodite"        . 4)
    ("apostrophe"       . 4)
    ("ariadne"          . 4)
    ("cafe"             . 2)
    ("calliope"         . 4)
    ("catastrophe"      . 4)
    ("chile"            . 2)
    ("chloe"            . 2)
    ("circe"            . 2)
    ("coyote"           . 3)
    ("daphne"           . 2)
    ("epitome"          . 4)
    ("eurydice"         . 4)
    ("euterpe"          . 3)
    ("every"            . 2)
    ("everywhere"       . 3)
    ("forever"          . 3)
    ("gethsemane"       . 4)
    ("guacamole"        . 4)
    ("hermione"         . 4)
    ("hyperbole"        . 4)
    ("jesse"            . 2)
    ("jukebox"          . 2)
    ("karate"           . 3)
    ("machete"          . 3)
    ("maybe"            . 2)
    ("newlywed"         . 3)
    ("penelope"         . 4)
    ("people"           . 2)
    ("persephone"       . 4)
    ("phoebe"           . 2)
    ("pulse"            . 1)
    ("queue"            . 1)
    ("recipe"           . 3)
    ("riverbed"         . 3)
    ("sesame"           . 3)
    ("shoreline"        . 2)
    ("simile"           . 3)
    ("snuffleupagus"    . 5)
    ("sometimes"        . 2)
    ("syncope"          . 3)
    ("tamale"           . 3)
    ("waterbed"         . 3)
    ("wednesday"        . 2)
    ("yosemite"         . 4)
    ("zoe"              . 2)

    ("abalones"          . 4)
    ("abares"            . 3)
    ("abbruzzeses"       . 4)
    ("abeds"             . 2)
    ("aborigines"        . 5)
    ("abruzzeses"        . 4)
    ("acreages"          . 3)
    ("adames"            . 3)
    ("adieus"            . 2)
    ("adobes"            . 3)
    ("anemones"          . 4)
    ("apaches"           . 3)
    ("aphrodites"        . 4)
    ("apostrophes"       . 4)
    ("ariadnes"          . 4)
    ("cafes"             . 2)
    ("calliopes"         . 4)
    ("catastrophes"      . 4)
    ("chiles"            . 2)
    ("chloes"            . 2)
    ("circes"            . 2)
    ("coyotes"           . 3)
    ("daphnes"           . 2)
    ("epitomes"          . 4)
    ("eurydices"         . 4)
    ("euterpes"          . 3)
    ("everys"            . 2)
    ("everywheres"       . 3)
    ("forevers"          . 3)
    ("gethsemanes"       . 4)
    ("guacamoles"        . 4)
    ("hermiones"         . 4)
    ("hyperboles"        . 4)
    ("jesses"            . 2)
    ("jukeboxes"         . 3)
    ("karates"           . 3)
    ("machetes"          . 3)
    ("maybes"            . 2)
    ("newlyweds"         . 3)
    ("penelopes"         . 4)
    ("peoples"           . 2)
    ("persephones"       . 4)
    ("phoebes"           . 2)
    ("pulses"            . 1)
    ("queues"            . 1)
    ("recipes"           . 3)
    ("riverbeds"         . 3)
    ("sesames"           . 3)
    ("shorelines"        . 2)
    ("similes"           . 3)
    ("snuffleupaguses"   . 6)
    ("sometimess"        . 2)
    ("syncopes"          . 3)
    ("tamales"           . 3)
    ("waterbeds"         . 3)
    ("wednesdays"        . 2)
    ("yosemites"         . 4)
    ("zoes"              . 2)
    ])

(define EXPRESSION_MONOSYLLABIC_ONE
  (irregex `(or (: "awe" (or eol "d" "so"))
                (: "cia" (or eol "l"))
                "tia"
                "cius"
                "cious"
                (: (~ ("aeiou")) "giu")
                (: ("aeiouy") (~ ("aeiouy")) "ion")
                "iou"
                (: "sia" eol)
                (: "eous" eol)
                (: ("oa") "gue" eol)
                (: any (>= 2 (~ ("aeiuoycgltdb"))) "ed" eol)
                (: any "ely" eol)
                (: bol "jua")
                "uai"
                "eau"
                (: bol "busi" eol)
                (: ("aeiouy")
                   (or ("bcfgklmnprsvwxyz")
                       "ch"
                       "dg"
                       (: "g" ("hn"))
                       "lch"
                       (: "l" ("lv"))
                       "mm"
                       "nch"
                       (: "n" ("cgn"))
                       (: "r" ("bcnsv"))
                       "squ"
                       (: "s" ("chkls"))
                       "th")
                   "ed"
                   eol)
                (: ("aeiouy")
                   (or ("bdfklmnprstvy")
                       "ch"
                       (: "g" ("hn"))
                       "lch"
                       (: "l" ("lv"))
                       "mm"
                       "nch"
                       "nn"
                       (: "r" ("nsv"))
                       "squ"
                       (: "s" ("cklst"))
                       "th")
                   "es"
                   eol))))

(define EXPRESSION_MONOSYLLABIC_TWO
  (irregex `(: ("aeiouy")
               (or ("bcdfgklmnprstvyz")
                   "ch"
                   "dg"
                   (: "g" ("hn"))
                   (: "l" ("lv"))
                   "mm"
                   (: "n" ("cgn"))
                   (: "r" ("cnsv"))
                   "squ"
                   (: "s" ("cklst"))
                   "th")
               "e"
               eol)))

(define EXPRESSION_DOUBLE_SYLLABIC_ONE
  (irregex `(: (or (: "(" (~ ("aeiouy")) ")\\1l") ;no clue what this is
                   (: (~ ("aeiouy")) "ie" (or "r" "st" "t"))
                   (: ("aeiouym") "bl")
                   "eo"
                   "ism"
                   "asm"
                   "thm"
                   "dnt"
                   "snt"
                   "uity"
                   "dea"
                   "gean"
                   "oa"
                   "ua"
                   (: "reac" (? "t"))
                   "orbed"
                   (: "eing" (? "s"))
                   (: ("aeiouy") "s" (? "h") "e" ("rs")))
               eol)))

(define EXPRESSION_DOUBLE_SYLLABIC_TWO
  (irregex `(or (: "creat" (neg-look-ahead "u"))
                (: (~ ("gq")) "ua" (~ ("auieo")))
                (= 3 ("aeiou"))
                (: bol (or "ia" "mc" (: "coa" ("dglx") any)))
                (: bol "re" (or "app" "es" "im" "us")))))

(define EXPRESSION_DOUBLE_SYLLABIC_THREE
  (irregex `(or (: (~ ("aeiou")) "y" ("ae"))
                (: (~ #\l) "lien")
                "riet"
                "dien"
                "iu"
                "io"
                "ii"
                "uen"
                (: ("aeilotu") "real")
                (: "real" ("aeilotu"))
                "iell"
                (: "eo" (~ ("aeiou")))
                (: ("aeiou") "y" ("aeiou")))))

(define EXPRESSION_DOUBLE_SYLLABIC_FOUR
  (irregex `(: (~ ("s")) "ia")))

(define EXPRESSION_SINGLE
  (irregex `(or (: bol
                   (or "un"
                       "fore"
                       "ware"
                       (: "non" (? "e"))
                       "out"
                       "post"
                       "sub"
                       "pre"
                       "pro"
                       "dis"
                       "side"
                       "some"))
                (: (or "ly"
                       "less"
                       "some"
                       "ful"
                       "ness"
                       (: "er"    (? "s"))
                       (: "cian"  (? "s"))
                       (: "ment"  (? "s"))
                       (: "ette"  (? "s"))
                       (: "ville" (? "s"))
                       (: "ship"  (? "s"))
                       (: "side"  (? "s"))
                       (: "port"  (? "s"))
                       (: "shire" (? "s"))
                       (: "tion"  (? "s"))
                       (: "tion"  (? "ed")))
                   eol))))

(define EXPRESSION_DOUBLE
  (irregex `(or (: bol
                   (or "above"
                       "anti"
                       "ante"
                       "counter"
                       "hyper"
                       "afore"
                       "agri"
                       "infra"
                       "intra"
                       "inter"
                       "over"
                       "semi"
                       "ultra"
                       "under"
                       "extra"
                       "dia"
                       "micro"
                       "mega"
                       "kilo"
                       "pico"
                       "nano"
                       "macro"
                       "somer"))
                (: (or "fully"
                       "berry"
                       "woman"
                       "women"
                       "edly")
                   eol))))

(define EXPRESSION_TRIPLE
  (irregex `(: (or "creations"
                   "creation"
                   "ology"
                   "ologist"
                   "onomy"
                   "onomist")
               eol)))

(define SPLIT                    (irregex 'eow))
(define APOSTROPHE               (irregex "[\"’']"))
(define EXPRESSION_NONALPHABETIC (irregex "[^a-z]+"))
(define EXPRESSION_CONSONANTS    (irregex "[^aeiouy]+"))

(define (count-occurences irx mult rem?)
  (lambda (str count)
    (values (if rem?
                (irregex-replace/all irx str "")
                str)
            (+ count
               (irregex-fold irx
                             (lambda (i m s)
                               (+ mult s))
                             0
                             str)))))

(define (count-gaps irx mult)
  (lambda (str count)
    (values str
            (+ count
               (* mult
                  (length (remove string-null?
                                  (irregex-split irx str))))))))

(define (compose-rev . z)
  (apply compose (reverse z)))

(define (syllable str)
  (cond ((= (string-length str) 0) 0)
        ((< (string-length str) 3) 1)
        ((assoc str problematic) (assoc-ref problematic str))
        (else (receive (str count)
                  [(compose-rev
                    (count-occurences EXPRESSION_TRIPLE 3 #t)
                    (count-occurences EXPRESSION_DOUBLE 2 #t)
                    (count-occurences EXPRESSION_SINGLE 1 #t)
                    (count-gaps EXPRESSION_CONSONANTS 1)
                    (count-occurences EXPRESSION_MONOSYLLABIC_ONE -1 #f)
                    (count-occurences EXPRESSION_MONOSYLLABIC_TWO -1 #f)
                    (count-occurences EXPRESSION_DOUBLE_SYLLABIC_ONE 1 #f)
                    (count-occurences EXPRESSION_DOUBLE_SYLLABIC_TWO 1 #f)
                    (count-occurences EXPRESSION_DOUBLE_SYLLABIC_THREE 1 #f)
                    (count-occurences EXPRESSION_DOUBLE_SYLLABIC_FOUR 1 #f))
                   str
                   0]
                count))))

(define (syllables str)
  (apply +
         (map (lambda (word)
                (syllable (irregex-replace/all EXPRESSION_NONALPHABETIC
                                               (string-downcase word)
                                               "")))
              (irregex-split SPLIT (irregex-replace APOSTROPHE str "")))))
