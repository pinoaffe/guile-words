(define-module (words distances)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (words utilities)
  #:export (hamming-distance
            damerau-levenshtein-distance
            levenshtein-distance
            subsequence-distance
            jaccard-similarity
            jaccard-distance
            ruzicka-similarity
            soergel-distance
            sorensen-dice-similarity
            sørensen-dice-similarity
            sorensen-dice-distance
            sørensen-dice-distance))

(define* (hamming-distance x y #:optional #:key (comparison equal?))
  "The hamming distance of two lists is the number of positions at
which the elements of the lists are different."
  (let ((x (listify x))
        (y (listify y)))
    (+ (abs (- (length x)
               (length y)))
       (length (remove identity
                       (map comparison
                            x
                            y))))))

(define* (count-edits x
                      y
                      #:optional #:key
                      (comparison equal?)
                      (substitution-allowed? #t)
                      (transposition-allowed? #t))
  (let ((x (listify x))
        (y (listify y)))
    (define helper
      (integer-memoize
       (lambda (i j)
         (cond ((= i 0) j)
               ((= j 0) i)
               (else (apply min
                            (filter identity
                                    (list
                                     (+ (helper i (- j 1)) 1)
                                     (+ (helper (- i 1) j) 1)
                                     (and transposition-allowed?
                                          (<= 2 i)
                                          (<= 2 j)
                                          (comparison (list-ref x (- i 1))
                                                      (list-ref y (- j 2)))
                                          (comparison (list-ref x (- i 2))
                                                      (list-ref y (- j 1)))
                                          (+ 1 (helper (- i 2) (- j 2))))
                                     (and (comparison (list-ref x (- i 1))
                                                      (list-ref y (- j 1)))
                                          (helper (- i 1)
                                                  (- j 1)))
                                     (and substitution-allowed?
                                          (+ 1 (helper (- i 1)
                                                       (- j 1))))))))))
       (length x)
       (length y)))
    (helper (length x) (length y))))

(define* (damerau-levenshtein-distance x y #:optional #:key (comparison equal?))
  "The Levenshtein-Damerau distance between two lists is the number of
insertions, deletions, substitutions and transpositions of two adjacent elements
required to make the lists equal."
  (count-edits x y
               #:comparison comparison
               #:substitution-allowed? #t
               #:transposition-allowed? #t))

(define* (levenshtein-distance x y #:optional #:key (comparison equal?))
  "The Levenshtein edit distance between two lists is the  number of insertions,
deletions and substitutions required to make the lists equal."
  (count-edits x y
               #:comparison comparison
               #:substitution-allowed? #t
               #:transposition-allowed? #f))

(define* (subsequence-distance x y #:optional #:key (comparison equal?))
  "The subsequence distance between two lists is the number of insertions
and deletions required to make the lists equal."
  (count-edits x y
               #:comparison comparison
               #:substitution-allowed? #f
               #:transposition-allowed? #f))

(define* (jaccard-similarity x y #:optional #:key (comparison equal?))
  "The Jaccard similarity of two sets is the ratio between the sizes of their
intersection and their union."
  (let* ((x (listify x))
         (y (listify y))
         (x-size (set-size x))
         (y-size (set-size y))
         (intersection-size
          (set-size
           (lset-intersection comparison x y))))
    (if (= 0 x-size y-size)
        0
        (/ intersection-size
           (- (+ x-size
                 y-size)
              intersection-size)))))

(define* (jaccard-distance x y #:optional #:key (comparison equal?))
  "The Jaccard distance of two sets is defined as 1 minus the jaccard
similarity of the sets."
  (- 1 (jaccard-similarity x y #:comparison comparison)))

(define soergel-distance jaccard-distance)
(define ruzicka-similarity jaccard-similarity)

(define* (sørensen-dice-similarity x y #:optional #:key (comparison equal?))
  "The Sørensen-Dice of two sets is twice the ratio between the sizes of their
intersection and the sum of their sizes."
  (let* ((x (listify x))
         (y (listify y))
         (x-size (set-size x))
         (y-size (set-size y))
         (intersection-size
          (set-size
           (lset-intersection comparison x y))))
    (if (= 0 x-size y-size)
        0
        (/ (* 2 intersection-size)
           (+ x-size
              y-size)))))

(define* (sørensen-dice-distance x y #:optional #:key (comparison equal?))
  "The Sørenson-Dice distance of two sets is defined as 1 minus the
Sørenson-Dice similarity of the sets.  Note: it is not a metric."
  (- 1 (sørensen-dice-similarity x y #:comparison comparison)))

(define sorensen-dice-similarity sørensen-dice-similarity)
(define sorensen-dice-distance sørensen-dice-distance)
